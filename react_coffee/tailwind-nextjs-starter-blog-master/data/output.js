export const output = [
   {
      "nom": "Pierre Louis FICQUET",
      "role": "Chef pilote",
      "nombre_sauvetages": "22 ",
      "equipage":"3",
      "date":"2021-10-25",
      "nombre_sauvees": "85 "
   },
   {
      "nom": "Mathias Bonnaventure Bommelaer",
      "role": "Chef pilote",
      "nombre_sauvetages": "5 ",
      "equipage":"3",
      "date":"2021-10-25",
      "nombre_sauvees": "16"
   },
   {
      "nom": "François Freed",
      "role": "Chef pilote",
      "nombre_sauvetages": "10 ",
      "equipage":"4",
      "date":"2021-10-25",
      "nombre_sauvees": "28"
   },
   {
      "nom": "François-André charlemain",
      "role": "Chef pilote",
      "nombre_sauvetages": "8 ",
      "equipage":"3",
      "date":"2021-10-25",
      "nombre_sauvees": "22"
   },
   {
      "nom": "André Bommelaer",
      "role": "Chef pilote",
      "nombre_sauvetages": "14 ",
      "equipage":"4",
      "date":"2021-10-25",
      "nombre_sauvees": "34"
   }
]

