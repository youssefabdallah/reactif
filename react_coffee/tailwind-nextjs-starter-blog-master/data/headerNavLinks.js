const headerNavLinks = [
  { href: '/Declaration', title: 'Declarations' },
  { href: '/Declaration/add', title: 'Nouveau Declaration', },
  { href: '/about', title: "à propos"}
]

export default headerNavLinks
