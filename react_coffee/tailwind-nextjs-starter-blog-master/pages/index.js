import Link from '@/components/Link'
import { PageSEO } from '@/components/SEO'
import Tag from '@/components/Tag'
import siteMetadata from '@/data/siteMetadata'
import { getAllFilesFrontMatter } from '@/lib/mdx'
import formatDate from '@/lib/utils/formatDate'
import { output } from '@/data/output'

import NewsletterForm from '@/components/NewsletterForm'

const MAX_DISPLAY = 5

export async function getStaticProps() {
  const posts = await getAllFilesFrontMatter('blog')

  return { props: { posts } }
}

export default function Home({ posts }) {
  return (
    <>
      <PageSEO title={siteMetadata.title} description={siteMetadata.description} />
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <h1 className="text-3xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
            Actualités 
          </h1>
          <p className="text-lg leading-7 text-gray-500 dark:text-gray-400">
            Consulter la liste des déclarations récement
          </p>
        </div>
        <ul className="divide-y divide-gray-200 dark:divide-gray-700">
        {output.map( post => (
             <li key={post.nom} className="py-4">
             <article className="space-y-2 xl:grid xl:grid-cols-4 xl:space-y-0 xl:items-baseline">
               <dl>
                 <dt className="sr-only">Published on</dt>
                 <dd className="text-base font-medium leading-6 text-gray-500 dark:text-gray-400">
                   <p>{post.date}</p>
                 </dd>
               </dl>
               <div className="space-y-3 xl:col-span-3">
                 <div>

                   <h5 className="text-2xl font-bold title leading-8 tracking-tight">
                     <span>Nom capitaine : </span>
                     <small className="small-grey-text">{post.nom}</small>
                   </h5>
                   
                   <h5 className="text-2xl font-bold title leading-8 tracking-tight">
                     <span>Nombre sauvetage :</span>
                     <small className="small-grey-text">{post.nombre_sauvetages}</small>
                   </h5>

                   <h5 className="text-2xl font-bold title leading-8 tracking-tight">
                     <span>Equipage :</span>
                     <small className="small-grey-text">{post.equipage}</small>
                   </h5>
                   <h5 className="text-2xl font-bold title leading-8 tracking-tight">
                     <span>Nombre sauvés :</span>
                     <small className="small-grey-text">{post.nombre_sauvees}</small>
                   </h5>
                 </div>
               </div>
             </article>
           </li>
          ))}
        </ul>
      </div>
      {posts.length > MAX_DISPLAY && (
        <div className="flex justify-end text-base font-medium leading-6">
          <Link
            href="/blog"
            className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
            aria-label="all posts"
          >
            Tous les déclarations &rarr;
          </Link>
        </div>
      )}
    </>
  )
}
