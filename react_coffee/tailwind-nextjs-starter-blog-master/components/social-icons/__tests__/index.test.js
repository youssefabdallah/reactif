
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import index from '..\index';


describe('index test', () => {
  it('index should match snapshot', () => {
    const component = renderer.create(<index
      size={"8"} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
