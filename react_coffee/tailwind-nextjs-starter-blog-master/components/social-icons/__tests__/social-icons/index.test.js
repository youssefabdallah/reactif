
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import social-icons\index from '..\social-icons\index';


describe('social-icons\index test', () => {
  it('social-icons\index should match snapshot', () => {
    const component = renderer.create(<social-icons\index
      size={"8"} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
