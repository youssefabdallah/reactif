
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import Giscus from '..\Giscus';


describe('Giscus test', () => {
  it('Giscus should match snapshot', () => {
    const component = renderer.create(<Giscus
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
