
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import Disqus from '..\Disqus';


describe('Disqus test', () => {
  it('Disqus should match snapshot', () => {
    const component = renderer.create(<Disqus
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
