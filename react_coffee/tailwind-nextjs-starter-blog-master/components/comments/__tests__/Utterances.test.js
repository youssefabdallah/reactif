
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import Utterances from '..\Utterances';


describe('Utterances test', () => {
  it('Utterances should match snapshot', () => {
    const component = renderer.create(<Utterances
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
