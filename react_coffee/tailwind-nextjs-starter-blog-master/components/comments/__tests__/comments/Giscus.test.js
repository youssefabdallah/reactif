
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import comments\Giscus from '..\comments\Giscus';


describe('comments\Giscus test', () => {
  it('comments\Giscus should match snapshot', () => {
    const component = renderer.create(<comments\Giscus
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
