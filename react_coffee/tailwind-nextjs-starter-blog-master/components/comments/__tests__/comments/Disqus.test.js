
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import comments\Disqus from '..\comments\Disqus';


describe('comments\Disqus test', () => {
  it('comments\Disqus should match snapshot', () => {
    const component = renderer.create(<comments\Disqus
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
