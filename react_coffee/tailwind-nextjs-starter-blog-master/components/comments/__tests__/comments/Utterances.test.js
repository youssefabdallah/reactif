
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import comments\Utterances from '..\comments\Utterances';


describe('comments\Utterances test', () => {
  it('comments\Utterances should match snapshot', () => {
    const component = renderer.create(<comments\Utterances
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
