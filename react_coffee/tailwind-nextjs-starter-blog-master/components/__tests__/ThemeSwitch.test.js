
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import ThemeSwitch from '..\ThemeSwitch';


describe('ThemeSwitch test', () => {
  it('ThemeSwitch should match snapshot', () => {
    const component = renderer.create(<ThemeSwitch
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
