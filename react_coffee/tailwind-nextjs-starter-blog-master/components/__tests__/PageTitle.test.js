
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import PageTitle from '..\PageTitle';


describe('PageTitle test', () => {
  it('PageTitle should match snapshot', () => {
    const component = renderer.create(<PageTitle
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
