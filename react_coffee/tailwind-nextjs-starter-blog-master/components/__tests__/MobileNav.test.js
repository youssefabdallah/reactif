
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import MobileNav from '..\MobileNav';


describe('MobileNav test', () => {
  it('MobileNav should match snapshot', () => {
    const component = renderer.create(<MobileNav
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
