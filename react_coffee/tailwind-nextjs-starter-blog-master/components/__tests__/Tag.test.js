
// Auto-generated do not edit


/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import Tag from '..\Tag';


describe('Tag test', () => {
  it('Tag should match snapshot', () => {
    const component = renderer.create(<Tag
       />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
